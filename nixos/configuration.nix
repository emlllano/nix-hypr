##mac m1 nixos machine


  { config, pkgs, ...
}: {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

#boot.kernelParams = [
#        ""
#    ];
boot.loader = {
  efi = {
    canTouchEfiVariables = false;
        };
  grub = {
     efiSupport = true;
     efiInstallAsRemovable = true;
     device = "nodev";
        };
    };

  #hostAndNetwork

  networking.hostName = "nixos";
  networking.networkmanager.enable = true;

  #timeZone.


  time.timeZone = "Europe/Paris";

  #locales

  i18n.defaultLocale = "en_US.UTF-8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "fr_FR.UTF-8";
    LC_IDENTIFICATION = "fr_FR.UTF-8";
    LC_MEASUREMENT = "fr_FR.UTF-8";
    LC_MONETARY = "fr_FR.UTF-8";
    LC_NAME = "fr_FR.UTF-8";
    LC_NUMERIC = "fr_FR.UTF-8";
    LC_PAPER = "fr_FR.UTF-8";
    LC_TELEPHONE = "fr_FR.UTF-8";
    LC_TIME = "fr_FR.UTF-8";
    };

  #autoUpdates

  system.autoUpgrade = {
	enable = true;
	channel = "https://nixos.org/channels/nixos-unstable";
    };


  #X11

  services.xserver.enable = true;
  services.xserver.displayManager.sddm = {
  enable = true;
    };

  # Configure keymap in X11

  services.xserver = {
    layout = "us";
    xkbVariant = "";
    };


  #print

  services.printing.enable = true;


  #pulseAudio

  sound.enable = true;
  hardware.pulseaudio.enable = true;
  hardware.pulseaudio.support32Bit = true;
  hardware.pulseaudio.extraConfig = "load-module module-combine-sink";
  security.rtkit.enable = true;


  #userPkgs:

  nixpkgs.config.permittedInsecurePackages = [
        "imagemagick-6.9.12-68"
    ];

  users.defaultUserShell = pkgs.fish;
  users.users.m1 = {
    isNormalUser = true;
    description = "m1";
    extraGroups = [
            "networkmanager" "audio" "wheel"
        ];
    packages = with pkgs; [
      chatgpt-cli

        ];
    };

  #userPkgs,unfree

 nixpkgs.config.allowUnfree = true;
 programs.dconf.enable = true;
 programs.fish.enable = true;
 programs.hyprland = {
      enable = true;
    } ;
  

  environment.systemPackages = with pkgs; [
  xdg-desktop-portal-hyprland
  wlprop
  wmctrl
  wlroots
  libsForQt5.polkit-kde-agent
  swaylock-effects
  swaybg
  waybar
  tofi
  xwayland
  wayland
  wayland-utils
  wlr-randr
  wtype
  wshowkeys
  wob
  imagemagick
  htop
  tmux
  vim
  util-linux
  font-manager
  font-awesome
  nerdfonts
  noto-fonts
  gawk
  pavucontrol
##  lm_sensors
  sysstat
  git
  neofetch
  alacritty
  wget
  breeze-icons
  breeze-gtk
  breeze-qt5
  capitaine-cursors
  lightly-qt
  qt5ct
  pavucontrol
  p7zip
  rar
    ];

  system.stateVersion = "23.05";
}
